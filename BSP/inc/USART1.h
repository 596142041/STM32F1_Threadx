#ifndef __USART1_H__
#define __USART1_H__
#include "bsp.h"
#define USART1_Tx_DMA 1
#define USART1_Rx_DMA 1
#define USER_USART USART1
#define USER_USART_GPIO GPIOA
#define USER_USART_Tx_Pin GPIO_Pin_10
#define USER_USART_Rx_Pin GPIO_Pin_9
#define USER_USART_Tx_DMA DMA1_Channel4
#define USER_USART_Rx_DMA DMA1_Channel5
void USART1_GPIO_Init(void);
void USART1_Init(uint32_t BaudRate);
void USART1_IT_Init(void);
void USART1_Rx_DMA_Init(uint8_t *pRx_data,uint16_t rx_len);
void USART1_Tx_DMA_Init(uint8_t *pTx_data,uint16_t rx_len);
#endif