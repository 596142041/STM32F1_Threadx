#ifndef __TIM1_H__
#define __TIM1_H__
#include "includes.h"
#define TIM1_PWM_CH1_GPIO GPIOA
#define TIM1_PWM_CH1_Pin  GPIO_Pin_7
#define TIM1_PWM_CH1N_Pin GPIO_Pin_8
#define TIM1_PWM_CH1_GPIO_CLK RCC_APB2Periph_GPIOA
void TIM1_PWM_GPIO_Init(void);
void TIM1_Init(void);
void TIM1_PWM_Init(void);
#endif