#ifndef __TIM2_H__
#define __TIM2_H__
#include "includes.h"
#define TIM2_PWM_CH1_GPIO GPIOA
#define TIM2_PWM_CH1_Pin GPIO_Pin_0
#define TIM2_PWM_CH2_Pin GPIO_Pin_1
#define TIM2_PWM_CH1_GPIO_CLK RCC_APB2Periph_GPIOA
void TIM2_GPIO_Init(void);
void TIM2_Init(void);
void TIM2_NVIC_Init(void);
void TIM2_PWM_Init(void);
void TIM2_Set_period(uint16_t period,uint16_t pulse);
#endif
