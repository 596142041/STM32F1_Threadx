#ifndef __USART_H__
#define __USART_H__
#include "stm32f10x.h"
#include "data.h"
#include <stdarg.h>
#include <stdio.h>
#ifndef __ICCARM__
int fputc(int ch, FILE *f);
#endif
static void USART_GPIO_Config(USART_TypeDef* USARTx);
void USART_Config(USART_TypeDef* USARTx,u32  USART_BaudRate);
void USART1_printf(USART_TypeDef* USARTx, uint8_t *Data,...);
#endif
