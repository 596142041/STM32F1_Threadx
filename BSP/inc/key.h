#ifndef __KEY_H__
#define __KEY_H__
#include "bsp.h"
#define KEY0_GPIO GPIOC
#define KEY0_Pin GPIO_Pin_5
#define Read_KEY0 GPIO_ReadInputDataBit(KEY0_GPIO,KEY0_Pin)

#define KEY1_GPIO GPIOA
#define KEY1_Pin GPIO_Pin_15
#define Read_KEY1 GPIO_ReadInputDataBit(KEY1_GPIO,KEY1_Pin)

#define WK_UP_GPIO GPIOA
#define WK_UP_Pin GPIO_Pin_0
#define Read_WK_UP() GPIO_ReadInputDataBit(WK_UP_GPIO,WK_UP_Pin)
void KEY_GPIO_Init(void);
#endif