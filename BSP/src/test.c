#include "test.h"
#define SET_INC GPIO_SetBits(GPIOA,GPIO_Pin_0)
#define CLR_INC GPIO_ResetBits(GPIOA,GPIO_Pin_0)
#define SET_U_D GPIO_SetBits(GPIOA,GPIO_Pin_1)
#define CLR_U_D GPIO_ResetBits(GPIOA,GPIO_Pin_1)
void TEST_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_init;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	GPIO_init.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_init.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1;
	GPIO_init.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_init);
}
void TEST_OUT(u16 data,u8 flag)
{
	u16 n;
	
	for(n = 0;n < data;n++)
	{
		if(flag  == 0)
		{
			CLR_U_D;
		}
		else 
		{
			SET_U_D;
		}
		SET_INC;
		delay_us(1);
		CLR_INC;
		delay_us(1);	
	}
	
}