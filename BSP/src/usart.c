#include "usart.h"
static void USART_GPIO_Config(USART_TypeDef* USARTx)
{
  GPIO_InitTypeDef GPIO_init;//声明GPIO配置结构体
  /*-----------------------USART GPIO 时钟配置--------------------------------*/
  if(USARTx == USART3)      
  {
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO|RCC_APB2Periph_GPIOB,ENABLE);//USART3是在GPIOB上面的,故使能GPIOB时钟和复用功能时钟
  }
  else if(USARTx == USART1)
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO|RCC_APB2Periph_GPIOA|RCC_APB2Periph_USART1,ENABLE);//USART1和USART2是在GPIOA上面的,故使能GPIOB时钟和复用功能时钟
  }
  else
  {
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO|RCC_APB2Periph_GPIOA,ENABLE);
  }
  /*------------------------------USART GPIO引脚功能配置
  模式:TX引脚配置为复用输出模式,RX配置为悬空输入模式
  引脚:USART1 TX : PA9 ,RX :PA10 ;USART2 TX : PA2 ,RX :PA3 ;
       USART3 TX : PB10 ,RX :PB11 ;
  ----------------------------------------------------------*/
  /*----------------TX引脚配置------------------------------*/
  GPIO_init.GPIO_Mode  = GPIO_Mode_AF_PP;    // 模式:复用功能输出
  GPIO_init.GPIO_Speed = GPIO_Speed_50MHz; // 引脚输出数据频率:有2M,10M,50M
  if(USARTx == USART3)
  {
    GPIO_init.GPIO_Pin = GPIO_Pin_10;
    GPIO_Init(GPIOB,&GPIO_init);
  }
  else if(USARTx == USART1)
  {
   GPIO_init.GPIO_Pin = GPIO_Pin_9;
    GPIO_Init(GPIOA,&GPIO_init); 
  }
  else
  {
    GPIO_init.GPIO_Pin = GPIO_Pin_2;
    GPIO_Init(GPIOA,&GPIO_init);
  }
  /*-----------------TX配置END--------------------------------*/
  /*----------------------------------------------------------*/
  /*-----------------RX配置-----------------------------------*/
  GPIO_init.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  if(USARTx == USART1)
  {
    GPIO_init.GPIO_Pin = GPIO_Pin_10;
    GPIO_Init(GPIOA,&GPIO_init);
  }
  else if(USARTx == USART2)
  {
   GPIO_init.GPIO_Pin = GPIO_Pin_3;
    GPIO_Init(GPIOA,&GPIO_init); 
  }
  else
  {
    GPIO_init.GPIO_Pin = GPIO_Pin_11;
    GPIO_Init(GPIOB,&GPIO_init);
  }
}
/*
函数名: void USART_Config(USART_TypeDef* USARTx,u32  USART_BaudRate);
输入参数: USARTx:x可为1,2,3;
          USART_BaudRate :波特率 ,通常用9600

返回值:无;
例如:  USART_Config(USART1,9600);
*/
void USART_Config(USART_TypeDef* USARTx,u32  USART_BaudRate)
{
  USART_InitTypeDef USART_init;//
	USART_ClockInitTypeDef USART_clock_init;
  
  USART_DeInit(USARTx);
  USART_GPIO_Config(USARTx);
	USART_init.USART_BaudRate            = USART_BaudRate;
	USART_init.USART_HardwareFlowControl = DISABLE;
	USART_init.USART_Mode                = USART_Mode_Rx|USART_Mode_Tx ;
	USART_init.USART_Parity              = USART_Parity_No;
	USART_init.USART_StopBits            = USART_StopBits_1;
	USART_init.USART_WordLength          = USART_WordLength_8b;
	USART_clock_init.USART_CPHA          = USART_CPHA_1Edge;
	USART_clock_init.USART_CPOL          = USART_CPOL_Low ;
	USART_clock_init.USART_Clock         = USART_Clock_Enable ;
	USART_clock_init.USART_LastBit       = USART_LastBit_Enable ;
	USART_Init(USARTx,&USART_init);
	USART_ClockInit(USARTx,&USART_clock_init);
  USART_Cmd(USARTx,ENABLE);
}
static char *itoa(int value, char *string, int radix)
{
	int     i, d;
	int     flag = 0;
	char    *ptr = string;
	
	/* This implementation only works for decimal numbers. */
	if (radix != 10)
	{
	    *ptr = 0;
	    return string;
	}
	
	if (!value)
	{
	    *ptr++ = 0x30;
	    *ptr = 0;
	    return string;
	}
	
	/* if this is a negative value insert the minus sign. */
	if (value < 0)
	{
	    *ptr++ = '-';
	
	    /* Make the value positive. */
	    value *= -1;
	}
	
	for (i = 10000; i > 0; i /= 10)
	{
	    d = value / i;
	
	    if (d || flag)
	    {
	        *ptr++ = (char)(d + 0x30);
	        value -= (d * i);
	        flag = 1;
	    }
	}
	
	/* Null terminate the string. */
	*ptr = 0;
	
	return string;

} 
void USART1_printf(USART_TypeDef* USARTx, uint8_t *Data,...)
{
	const char *s;
	int d;   
	char buf[16];
	
	va_list ap;
	va_start(ap, Data);
	
	while ( *Data != 0)     
	{				                          
		if ( *Data == 0x5c )  
	{									  
	switch ( *++Data )
	{
		case 'r':							        
			USART_SendData(USARTx, 0x0d);
			Data ++;
		break;
		
		case 'n':							         
			USART_SendData(USARTx, 0x0a);	
			Data ++;
		break;
		
		default:
			Data ++;
		break;
	}			 
	}
	else if ( *Data == '%')
	{									 
	switch ( *++Data )
	{				
		case 's':										 
			s = va_arg(ap, const char *);
	for ( ; *s; s++) 
	{
		USART_SendData(USARTx,*s);
		while( USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET );
	}
		Data++;
		break;
	
	case 'd':										 
	d = va_arg(ap, int);
	itoa(d, buf, 10);
	for (s = buf; *s; s++) 
	{
		USART_SendData(USARTx,*s);
		while( USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET );
	}
	Data++;
	break;
		 default:
				Data++;
		    break;
	}		 
	} /* end of else if */
	else USART_SendData(USARTx, *Data++);
	while( USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET );
	}
}
#ifndef __ICCARM__
int fputc(int ch, FILE *f)
{
	USART_SendData(USART1, (unsigned char) ch);
	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
	return (ch);
}
#endif


