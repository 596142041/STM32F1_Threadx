#include "TIM2.h"
#define PWM_40KHz 900
#define PWM_25KHz 1440
#define TIM2_period PWM_25KHz
void TIM2_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_init;
	GPIO_init.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_init.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_init.GPIO_Pin = TIM2_PWM_CH1_Pin|TIM2_PWM_CH2_Pin;
	GPIO_Init(TIM2_PWM_CH1_GPIO,&GPIO_init);
	GPIO_init.GPIO_Mode = GPIO_Mode_Out_PP;
	
	GPIO_init.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOA,&GPIO_init);
	GPIO_ResetBits(GPIOA,GPIO_Pin_3);
}
void TIM2_Init(void)
{
	TIM_TimeBaseInitTypeDef Tim2_Init;
	
	Tim2_Init.TIM_CounterMode = TIM_CounterMode_Up;
	Tim2_Init.TIM_Prescaler = 1-1;
	Tim2_Init.TIM_Period = TIM2_period;
	TIM_TimeBaseInit(TIM2,&Tim2_Init);
	TIM_ARRPreloadConfig(TIM2,ENABLE);
	TIM_Cmd(TIM2,ENABLE);
}
void TIM2_PWM_Init(void)
{
	TIM_OCInitTypeDef TIM2_PWM_init;
	TIM2_PWM_init.TIM_OCMode      = TIM_OCMode_PWM1;
	TIM2_PWM_init.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM2_PWM_init.TIM_OCPolarity  = TIM_OCPolarity_High;
	TIM2_PWM_init.TIM_OutputState = TIM_OutputState_Enable;
	TIM2_PWM_init.TIM_Pulse       = 680;
	TIM_OC1Init(TIM2,&TIM2_PWM_init);

	TIM2_PWM_init.TIM_Pulse  = TIM2_period-680;
	TIM2_PWM_init.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OC2Init(TIM2,&TIM2_PWM_init);
}
void TIM2_NVIC_Init(void)
{
	NVIC_InitTypeDef NVIC_init;
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
	NVIC_init.NVIC_IRQChannel    = TIM2_IRQn;
	NVIC_init.NVIC_IRQChannelCmd = ENABLE;
	NVIC_init.NVIC_IRQChannelPreemptionPriority = 0x01;
	NVIC_init.NVIC_IRQChannelSubPriority  =0x00;
	NVIC_Init(&NVIC_init);
}
void TIM2_Set_period(uint16_t period,uint16_t pulse)
{
	TIM_SetAutoreload(TIM2,period);
	TIM_SetCompare1(TIM2,pulse);
	TIM_SetAutoreload(TIM2,period);
	TIM_SetCompare2(TIM2,pulse);
}