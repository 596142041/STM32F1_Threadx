#include "bsp.h"
#include "bsp_dwt.h"
void Bsp_Init(void)
{
	//所有与外设时钟统一在此处进行使能
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1|RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO|RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOC,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	bsp_InitDWT();
	TIM1_PWM_GPIO_Init();
	TIM2_GPIO_Init();
	TIM1_Init();
	TIM1_PWM_Init();
	TIM2_Init();
	TIM2_PWM_Init();
	USART1_Init(9600);
	GPIO_PinRemapConfig(GPIO_PartialRemap_TIM1,ENABLE);
}