#include "key.h"
void KEY_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_init;
	GPIO_init.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_init.GPIO_Pin = KEY0_Pin;
	GPIO_Init(KEY0_GPIO,&GPIO_init);
	GPIO_init.GPIO_Pin = KEY1_Pin;
	GPIO_Init(KEY1_GPIO,&GPIO_init);
	GPIO_init.GPIO_Pin = WK_UP_Pin;
	GPIO_Init(WK_UP_GPIO,&GPIO_init);
}