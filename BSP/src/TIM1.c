#include "TIM1.h"
void TIM1_PWM_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_init;
	GPIO_init.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_init.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_init.GPIO_Pin = GPIO_Pin_7|GPIO_Pin_8;
	GPIO_Init(GPIOA,&GPIO_init);
}
void TIM1_Init(void)
{
	TIM_TimeBaseInitTypeDef TIM_init;
	TIM_TimeBaseStructInit(&TIM_init);
	TIM_DeInit(TIM1);
	TIM_init.TIM_CounterMode  =TIM_CounterMode_Up;
	TIM_init.TIM_Period = 1000;
	TIM_init.TIM_Prescaler = 0;
	TIM_init.TIM_ClockDivision = 0;
	TIM_TimeBaseInit(TIM1,&TIM_init);
	TIM_ARRPreloadConfig(TIM1,ENABLE);
	TIM_Cmd(TIM1,ENABLE);
}
void TIM1_PWM_Init(void)
{
	TIM_OCInitTypeDef TIM_PWM;
	TIM_BDTRInitTypeDef TIM1_BDTR;
	TIM_BDTRStructInit(&TIM1_BDTR);
	TIM_OCStructInit(&TIM_PWM);
	TIM_PWM.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM_PWM.TIM_OCMode  = TIM_OCMode_PWM1;
	TIM_PWM.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_PWM.TIM_OCNPolarity  =TIM_OCNPolarity_High;
	TIM_PWM.TIM_OCPolarity  =TIM_OCPolarity_High;
	TIM_PWM.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_PWM.TIM_OutputState = TIM_OutputState_Enable;
	TIM_PWM.TIM_Pulse = 100;
	TIM_OC1Init(TIM1,&TIM_PWM);
	TIM1_BDTR.TIM_Break = TIM_Break_Disable;
	TIM1_BDTR.TIM_BreakPolarity = TIM_BreakPolarity_Low;
	TIM1_BDTR.TIM_DeadTime = 3;
	TIM1_BDTR.TIM_LOCKLevel = TIM_LOCKLevel_OFF;
	TIM1_BDTR.TIM_OSSIState = TIM_OSSIState_Enable;
	TIM1_BDTR.TIM_OSSRState = TIM_OSSRState_Enable;
	TIM1_BDTR.TIM_AutomaticOutput = TIM_AutomaticOutput_Disable;
	TIM_BDTRConfig(TIM1,&TIM1_BDTR);
	TIM_CtrlPWMOutputs(TIM1,ENABLE);
}