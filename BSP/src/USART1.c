#include "usart1.h"
void USART1_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_init;
	
	GPIO_init.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_init.GPIO_Pin  =USER_USART_Tx_Pin;
	GPIO_init.GPIO_Speed  =GPIO_Speed_50MHz;
	GPIO_Init(USER_USART_GPIO,&GPIO_init);
	GPIO_init.GPIO_Pin = USER_USART_Rx_Pin;
	GPIO_init.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(USER_USART_GPIO,&GPIO_init);
}
void USART1_Init(uint32_t BaudRate)
{
	USART_InitTypeDef USART_init;
	USART1_GPIO_Init();
	USART_init.USART_BaudRate = BaudRate;
	USART_init.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_init.USART_Mode  =USART_Mode_Rx|USART_Mode_Tx;
	USART_init.USART_Parity = USART_Parity_No;
	USART_init.USART_StopBits = USART_StopBits_1;
	USART_init.USART_WordLength = USART_WordLength_8b;
	USART_Init(USER_USART,&USART_init);
	USART_Cmd(USER_USART,ENABLE);
	#if USART1_Rx_DMA == 1
	
	#endif
}
void USART1_IT_Init(void)
{
	
}
void USART1_Rx_DMA_Init(uint8_t *pRx_data,uint16_t rx_len)
{
	
}
void USART1_Tx_DMA_Init(uint8_t *pTx_data,uint16_t rx_len)
{
	
}