#include "config.h"
#ifdef USE_GPIO
static void GPIO_RCC_Config(GPIO_TypeDef* GPIOx)
{
  if(GPIOx == GPIOA)
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
  }
  else if (GPIOx == GPIOB)
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
  }
  else if (GPIOx == GPIOC)
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
  }
  else if (GPIOx == GPIOD)
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD,ENABLE);
  }
  else if (GPIOx == GPIOE)
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE,ENABLE);
  }
  else if (GPIOx == GPIOF)
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF,ENABLE);
  }
  else if (GPIOx == GPIOG)
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG,ENABLE);
  }
}
void GPIO_Configuration(GPIO_TypeDef* GPIOx,
									uint16_t GPIO_Pin,
									GPIOMode_TypeDef GPIO_Mode,
									GPIOSpeed_TypeDef GPIO_Speed
								)
{
  GPIO_InitTypeDef GPIO_init;
  GPIO_RCC_Config(GPIOx);
  GPIO_init.GPIO_Pin = GPIO_Pin;
  GPIO_init.GPIO_Mode = GPIO_Mode;
  GPIO_init.GPIO_Speed = GPIO_Speed;
   GPIO_Init(GPIOx,&GPIO_init);
}
#endif

#ifdef USE_NVIC
void NVIC_Configuration(uint32_t NVIC_PriorityGroup,
                 uint8_t NVIC_IRQChannel,
                 uint8_t NVIC_IRQChannelPreemptionPriority,
                 uint8_t NVIC_IRQChannelSubPriority,
                 FunctionalState NVIC_IRQChannelCmd
                )
{
    NVIC_InitTypeDef NVIC_INIT;
    NVIC_PriorityGroupConfig (NVIC_PriorityGroup);
    NVIC_INIT.NVIC_IRQChannel = NVIC_IRQChannel;
    NVIC_INIT.NVIC_IRQChannelPreemptionPriority = NVIC_IRQChannelPreemptionPriority;
    NVIC_INIT.NVIC_IRQChannelSubPriority = NVIC_IRQChannelSubPriority;
    NVIC_INIT.NVIC_IRQChannelCmd  = NVIC_IRQChannelCmd;
    NVIC_Init(&NVIC_INIT);
}
#endif

#ifdef USE_EXTI
void EXTI_Configuration(uint32_t EXTI_Line,
				 EXTIMode_TypeDef EXTI_Mode,
				 EXTITrigger_TypeDef EXTI_Trigger,
				 FunctionalState EXTI_LineCmd
				)
{
	EXTI_InitTypeDef EXTI_init;
	EXTI_init.EXTI_Line = EXTI_Line;
	EXTI_init.EXTI_Mode = EXTI_Mode;
	EXTI_init.EXTI_Trigger  = EXTI_Trigger;
	EXTI_init.EXTI_LineCmd = EXTI_LineCmd;
	EXTI_Init(&EXTI_init);
}
#endif
#ifdef USE_PWM
void PWM_OUTPUT_Config(TIM_TypeDef* TIMx,
                       CHANLE chanle,
                       float pinglv,
                       uint16_t TIM_Prescaler,
                       float TIM_Pulse
                      )
{
  float ARR;//ARR的值
  TIM_TimeBaseInitTypeDef TIM_time_base;//定时器基本初始化
  TIM_OCInitTypeDef TIM_OC_init;// PWM输出初始化  
  assert_param(check(TIMx));// 参数检查;
  assert_param(Check(TIM_Pulse));
  /*外设时钟使能*/
  if(TIMx == TIM2)
  {
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
  }
  else if(TIMx == TIM3)
  {
     RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
  }
   else if(TIMx == TIM4)
  {
     RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
  }
  /*对ARR计算*/
  ARR = 72000000/(pinglv*TIM_Prescaler);
  /**************************
   *   定时器基本配置;
   *对频率,计数模式的设置;
  ***************************/
  TIM_time_base.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_time_base.TIM_Prescaler   = TIM_Prescaler-1;
  TIM_time_base.TIM_Period      = (u16)ARR-1;
  TIM_ARRPreloadConfig(TIMx,ENABLE);
  TIM_TimeBaseInit(TIMx,&TIM_time_base);
  TIM_Cmd(TIMx,ENABLE);
  /*基本配置结束*/
  /***************************
   *PWM输出配置
   *占空比,模式
   *
   ***************************/
  if(TIM_Pulse > 1.0)
    TIM_Pulse = 1.0;//参数检查
  TIM_OC_init.TIM_Pulse = (u16)(ARR*TIM_Pulse);
  TIM_OC_init.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OC_init.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OC_init.TIM_OCIdleState = TIM_OCIdleState_Set;
  TIM_OC_init.TIM_OutputState = TIM_OutputState_Enable;
  if(chanle == chanle_1)
  {
    TIM_OC1Init(TIMx,&TIM_OC_init);
    TIM_OC1PreloadConfig(TIMx,TIM_OCPreload_Enable);
  }
  else if (chanle == chanle_2)
  {
    TIM_OC2Init(TIMx,&TIM_OC_init);
     TIM_OC2PreloadConfig(TIMx,TIM_OCPreload_Enable);
  }
  else if (chanle == chanle_3)
  {
    TIM_OC3Init(TIMx,&TIM_OC_init);
     TIM_OC3PreloadConfig(TIMx,TIM_OCPreload_Enable);
  }
  else if (chanle == chanle_4)
  {
    TIM_OC4Init(TIMx,&TIM_OC_init);
     TIM_OC4PreloadConfig(TIMx,TIM_OCPreload_Enable);
  }
  TIM_CtrlPWMOutputs(TIMx,ENABLE);
}

#endif
