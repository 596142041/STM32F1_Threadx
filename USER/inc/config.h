#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "stm32f10x.h"
#include "data.h"
#include "delay.h"
#include "stdio.h"
#define check(PERIPH)                (((PERIPH) == TIM2) || \
                                      ((PERIPH) == TIM3) || \
                                      ((PERIPH) == TIM4)) 
#define  Check(value)                ((value>=0)&&(value=<1.0))
#define AFIO_RCC_ON   RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE) 
#define PWM_ON(TIMx)  TIM_CtrlPWMOutputs(TIMx,ENABLE)                  
typedef enum {
   chanle_1 = 0x01,
   chanle_2 = 0x02,
   chanle_3 = 0x03,
   chanle_4 = 0x04,
} CHANLE;
typedef enum
{
	FALSE = 0,
	TRUE  = 1
}Bool;
typedef struct 
{
	void (*GPIO)(void);
	void (*Config)(void);
	void (*PWM_Config)(void);
	void (*NVIC_Config)(void);
} timer;
#define USE_GPIO
#define USE_PWM
#define USE_EXTI
#define USE_NVIC
#ifdef USE_PWM
void PWM_OUTPUT_Config(TIM_TypeDef* TIMx,
                       CHANLE chanle,
                       float pinglv,
                       uint16_t TIM_Prescaler,
                       float  TIM_Pulse
                      );
#endif
#ifdef USE_GPIO
static void GPIO_RCC_Config(GPIO_TypeDef* GPIOx);
void GPIO_Configuration(GPIO_TypeDef* GPIOx,
									uint16_t GPIO_Pin,
									GPIOMode_TypeDef GPIO_Mode,
									GPIOSpeed_TypeDef GPIO_Speed
								);
#endif
#ifdef USE_NVIC
void NVIC_Configuration(uint32_t NVIC_PriorityGroup,
								uint8_t NVIC_IRQChannel,
								uint8_t NVIC_IRQChannelPreemptionPriority,
								uint8_t NVIC_IRQChannelSubPriority,
								FunctionalState NVIC_IRQChannelCmd
							  );
#endif
#ifdef USE_EXTI
void EXTI_Configuration(uint32_t EXTI_Line,
				 EXTIMode_TypeDef EXTI_Mode,
				 EXTITrigger_TypeDef EXTI_Trigger,
				 FunctionalState EXTI_LineCmd
				);	
#endif

#endif
