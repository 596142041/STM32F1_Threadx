#ifndef __DATA_H__
#define __DATA_H__
#include "stm32f10x.h"
typedef unsigned short int uint;
typedef unsigned long int uintl;
typedef unsigned char uchar;
#define u8 unsigned char
#define u16 unsigned short  int
#define u32 unsigned long int
#endif
